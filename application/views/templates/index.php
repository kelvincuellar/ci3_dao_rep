<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>{title}</title>
</head>
<body>
	<div class="container">
		{header}
		
		{content}
		
		{footer}
	</div>
</body>
</html>