<?php
defined('BASEPATH') or exit('Sin control para accesar');

class CarteraClientes extends CI_Controller{
	public function __construct(){
		parent::__construct();
	}

	/*Fn: Mostrar vista principal de CarteraClientes
	@param: na
	@return: vista principal*/
	public function index(){
		$this->load->view('templates/header');
		$this->load->view('clientes');
		$this->load->view('templates/footer');
	}
}