<?php
defined('BASEPATH') or exit('No es permitido accesar');
//Class: Implementa el modelo de factura
class FacturaImp extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->library('Factura');
	}

	/*Gestion*/
	/*Fn: Agrega una factura
	@param: Objeto factura
	@return:na
	*/
	public function create($factura){

	}

	/*Fn: Devuelve un listado de facturas
	@param:na
	@return: Lista de facturas
	*/
	public function read(){
		
	}

	/*Fn: Obtiene una factura
	@param: id factura
	@return: Instancia de factura
	*/
	public function readById($id){
		
	}

	/*Fn: Actualiza los datos de factura
	@param: Objeto factura
	@return:na
	*/
	public function update($factura){

	}

	/*Fn: Quita factura de la bd
	@param: id a quitar
	@return:na
	*/
	public function delete($id){

	}
}