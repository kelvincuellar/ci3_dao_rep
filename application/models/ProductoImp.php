<?php
defined('BASEPATH') or exit('No es permitido accesar');
//Class: Implementa el modelo de producto
class ProductoImp extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->library('Producto');
	}

	/*Gestion*/
	/*Fn: Agrega una producto
	@param: Objeto producto
	@return:na
	*/
	public function create($producto){

	}

	/*Fn: Devuelve un listado de productos
	@param:na
	@return: Lista de productos
	*/
	public function read(){
		
	}

	/*Fn: Obtiene una producto
	@param: id producto
	@return: Instancia de producto
	*/
	public function readById($id){
		
	}

	/*Fn: Actualiza los datos de producto
	@param: Objeto producto
	@return:na
	*/
	public function update($producto){

	}

	/*Fn: Quita producto de la bd
	@param: id a quitar
	@return:na
	*/
	public function delete($id){

	}
}