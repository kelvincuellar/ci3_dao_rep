<?php
defined('BASEPATH') or exit('No es permitido accesar');
//Class: Implementa el modelo de categoria de productos
class CategoriaImp extends CI_Model{

	public function __construct(){
		parent::__construct();
		$this->load->library('Categoria');
	}

	/*Gestion*/
	/*Fn: Agrega una categoria
	@param: Objeto categoria
	@return:na
	*/
	public function create($categoria){

	}

	/*Fn: Devuelve un listado de categorias
	@param:na
	@return: Lista de categorias
	*/
	public function read(){
		return $this->db->get('categoria')->result();
	}

	/*Fn: Obtiene una categoria
	@param: id de la categoria
	@return: Instancia de categoria
	*/
	public function readById($id=1){
		$ax = $this->db->get_where('categoria',array('id_categoria'=>$id))->row();
		$x = new Categoria();
		$x->setNombre($ax->nombre);
		$x->setDescripcion($ax->descripcion);
		return $x;
	}

	/*Fn: Actualiza los datos de una categoria
	@param: Objeto categoria
	@return:na
	*/
	public function update($categoria){

	}

	/*Fn: Quita una categoria de la bd
	@param: id a quitar
	@return:na
	*/
	public function delete($id){

	}

	/*Metodo para unir arrays

	$publicar=array();
    foreach ($rows as $key => $object) {
        $publicar[]= array (
                            'unidad_de_medida'=$object->product_type, 
                            'codigo'=$object->second_name
                           );
    }

	*/
}