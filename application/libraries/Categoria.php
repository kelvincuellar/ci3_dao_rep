<?php

class Categoria{

	private $nombre;
	private $descripcion;
	//Array de instancias
	private $productos;

	/*Rel*/
	public function getProductos(){
		return $this->productos;
	}

	public function setProductos($productos){
		$this->productos = $productos;
	}

	/*G&S*/
	public function getNombre(){
		return $this->nombre;
	}

	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	public function getDescripcion(){
		return $this->descripcion;
	}

	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}
}