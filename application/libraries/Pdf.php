<?php defined('BASEPATH') OR exit('No direct script access allowed');
//Importando namespace
use Dompdf\Dompdf;

class Pdf{
  public function __construct(){
    require_once dirname(__FILE__).'\..\third_party\dompdf\autoload.inc.php';
    $pdf = new DOMPDF();
    $CI = & get_instance();
    $CI->dompdf = $pdf;
  }
}