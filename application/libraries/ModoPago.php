<?php

class ModoPago{

	private $nombre;
	private $otrosDetalles;
	//Array de instancias
	private $facturas;

	/*Rel*/
	public function getFacturas(){
		return $this->facturas;
	}

	public function setFacturas($facturas){
		$this->facturas = $facturas;
	}

	/*G&S*/
	public function getNombre(){
		return $this->nombre;
	}

	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	public function getOtrosDetalles(){
		return $this->otrosDetalles;
	}

	public function setOtrosDetalles($otrosDetalles){
		$this->otrosDetalles = $otrosDetalles;
	}
}