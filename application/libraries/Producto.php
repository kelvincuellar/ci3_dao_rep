<?php

class Producto{

	private $nombre;
	private $precio;
	private $stock;
	//Instancia foranea de pertenencia
	private $categoria;
	//Array de instancias
	private $detalles;

	/*Rel*/
	public function getCategoria(){
		return $this->categoria;
	}

	public function setCategoria($categoria){
		$this->categoria = $categoria;
	}

	public function getDetalles(){
		return $this->detalles;
	}

	public function setDetalles($detalles){
		$this->detalles = $detalles;
	}

	/*G&S*/
	public function getNombre(){
		return $this->nombre;
	}

	public function setNombre($nombre){
		$this->nombre = $nombre;
	}

	public function getPrecio(){
		return $this->precio;
	}

	public function setPrecio($precio){
		$this->precio = $precio;
	}

	public function getStock(){
		return $this->stock;
	}

	public function setStock($stock){
		$this->stock = $stock;
	}
}