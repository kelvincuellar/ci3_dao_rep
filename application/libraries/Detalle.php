<?php

class Detalle{

	private $cantidad;
	private $precio;
	//Instancias foraneas para definir el detalle
	private $factura;
	private $producto;

	/*Rel*/
	public function getFactura(){
		return $this->factura;
	}

	public function setFactura($factura){
		$this->factura = $factura;
	}

	public function getProducto(){
		return $this->producto;
	}

	public function setProducto($producto){
		$this->producto = $producto;
	}

	/*G&S*/
	public function getCantidad(){
		return $this->cantidad;
	}

	public function setCantidad($cantidad){
		$this->cantidad = $cantidad;
	}

	public function getPrecio(){
		return $this->precio;
	}

	public function setPrecio($precio){
		$this->precio = $precio;
	}
}